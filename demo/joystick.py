#!/usr/bin/env python3
import time
import struct
from numpy import clip
from sled import Sled

xMax = 0.10
vMax = 0.60
with Sled() as sled:
	# Set target velocity
	sled.node.sdo[0x60ff].raw = 0 # um/s
	t0 = time.time()
	v = 0
	with open("/dev/input/js1", "rb") as f:
		while True:
			a = f.read(8)
			t, value, code, index = struct.unpack("<Ihbb", a) # 4 bytes, 2 bytes, 1 byte, 1 byte
			# t: time in ms
			# index: button/axis number (0 for x-axis)
			# code: 1 for buttons, 2 for axis, -127 for buttons init, -126 for axis init
			# value: axis position, 0 for center, 1 for buttonpress, 0 for button release
			#print("t: {:10d} ms, value: {:6d}, code: {:1d}, index: {:1d}".format(t, value, code, index))
			#code %= 128 # treat initialization as normal values
			
			if code == 2 and index == 0: # x-axis !
				v = vMax * float(value)/2**15
				print("{:8.3f}: j_x: {:6d}, v_d: {:6.3f}".format(time.time()-t0, value, v))
			v = clip(v, -vMax, vMax)
			sled.velocity(v)  # um/s


