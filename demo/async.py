#!/usr/bin/env python3
import time
import logging 
from sled import Sled

logging.basicConfig(level=logging.INFO)

with Sled() as sled:
	t0 = time.time()
	def print_position(m):
		"""
		show position when it changes at maximum 1000Hz 
		"""
		t1 = time.time()
		print("{:9.6f}\t{:9.6f}".format(t1-t0, m[1].phys*1e-6))
		
	sled.node.tpdo[3].add_callback(print_position)
	sled.start_sinusoid(0.200, 4.0)
	time.sleep(4.0)
	sled.stop_sinusoid()
	time.sleep(4.0)
