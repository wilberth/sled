#!/usr/bin/env python3
import time
import logging 
from sled import Sled

logging.basicConfig(level=logging.INFO)

with Sled() as sled:
	print("x: {:.3f}".format(sled.x))
	v = 0.1 # m/s
	sled.velocity(v)  
	time.sleep(1)
	print("x: {:.3f}".format(sled.x))
	sled.velocity(-v)  # um/s
	time.sleep(1)
	print("x: {:.3f}".format(sled.x))
	sled.velocity(0.01) # no need to stop explicitly, context manager will stop motion


