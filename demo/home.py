#!/usr/bin/env python3
import logging 
from sled import Sled
logging.basicConfig(level=logging.INFO)

with Sled() as sled:
	# explicit homing should not be necessary, 
	# run this script if homing does not work
	sled.init_home(force=True)
