#!/usr/bin/env python3
import time
import logging 
from sled import Sled
logging.basicConfig(level=logging.INFO)

with Sled() as sled:
	# positive absolute position motion
	sled.move(0.200) # todo: too fast motion gives error
	# negative relative motion
	sled.move(-0.200, relative=True)
	
	# fixed move time
	sled.move(0.200, dt=2)
	# negative relative motion
	sled.move(-0.200, dt=2, relative=True)
