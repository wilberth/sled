#!/usr/bin/env python3
import time
import logging 
from sled import Sled
logging.basicConfig(level=logging.INFO)

with Sled() as sled:
	# warning, sinusoid is async, use a sleep after it
	sled.start_sinusoid(0.200, 4.0)
	t0 = time.time()
	for t in range(50):
		print("{:6.3f}\t{:6.3f}\t{:6.3f}".
			format(time.time()-t0, sled.x, sled.v))
		time.sleep(0.1)
	# sled.x is very accurate, updated at 1000 Hz
	#sled.stop_sinusoid() # no need to stop explicitly, context manager will stop motion

