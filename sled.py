#!/usr/bin/env python3

# Copyright (C) 2019-2023 Wilbert van Ham, Radboud University Nijmegen
# Copyright (C) 2024 Sophie Willemsen, Radboud University Nijmegen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import time
import logging
import canopen
from canopen.profiles.p402 import BaseNode402
# todo:
# prevent too fast
# wait for ack after command rather then wait 100ms
# automatically home if not homed (determine this from error status if command fails)
class Sled():
	max_velocity = 1.0 # m/s, maximum velocity in velocity mode
	move_timeout = 1 # s maximum time for move above set time
	homing_timeout = 40 # s maximum time for homing
	minimum_move = 0.100e-3 # m, ignore very small moves
	v_factor = 44.82 # velocity reported is this factor to large, don't know why

	virgin = True # no motion yet, neither PP, PV, HOME
	def __init__(self, channel='can0', reset=False):
		"""
		channel: socketcan socket 
		reset: will always require green button press for start
		"""
		self.network = canopen.Network()

		# Connect to the CAN bus
		self.network.connect(bustype='socketcan', channel=channel)
		self.network.check()
		self.network.scanner.search()
		time.sleep(0.05)
		logging.info("Network nodes found: {}".format(self.network.scanner.nodes))
		if len(self.network.scanner.nodes)==0:
			logging.error("No network nodes found. Is the canable connected to the sled? Is the Sled powered on?")

		# Add node with its Object Dictionaries
		self.node = BaseNode402(1, "/home/experiment/Documents/Wilbert/sled" + "/" + "s300s700.eds")
		self.network.add_node(self.node)
		time.sleep(.2)
		self.control = 0x06 # shutdown -> READY TO SWITCH ON
		time.sleep(.1)
		self.control = 0x07 # switch on -> SWITCHED ON
		time.sleep(.1)

		# Load the configuration of the node from the object dictionary
		try:
			self.node.load_configuration()
		except Exception as e:
			logging.error("No network node found. Did you power on the sled?")
			raise e

		# Print device specific info
		logging.info("Device name: {}".format(self.node.sdo[0x1008].raw))
		logging.info("Vendor id: {}".format(self.node.sdo[0x1018][1].raw))
		logging.info("Product code: {:x}".format(self.node.sdo[0x1018][2].raw))

		if not self.network:
			raise RuntimeError("Failure: not connected to motor controller")

		if reset:
			logging.info("Node nmt state pre reset= {0}".format(self.node.nmt.state)) # INITIALISING
			self.node.nmt.state = 'RESET'
			self.node.nmt.wait_for_bootup(15) # canopen.nmt.NmtError: Timeout waiting for boot-up message
			logging.info("Node nmt state post reset= {0}".format(self.node.nmt.state)) # PRE-OPERATIONAL

		logging.info("Node nmt state = {}".format(self.node.nmt.state))
		self.node.nmt.state = 'RESET COMMUNICATION'
		logging.info("Node nmt state = {}".format(self.node.nmt.state))
		self.node.nmt.state = 'PRE-OPERATIONAL'
		logging.info("Node nmt state = {}".format(self.node.nmt.state))
		
		#self.node.reset_from_fault() # timeout 
		logging.info("Node nmt state = {}".format(self.node.nmt.state))
		
		# now change the PDO mapping before going to operational mode
		# 4 was: Statusword (0x6041:0, 16 bits) and Velocity actual value (0x606C:0, 32 bits)
		# 3 was: Statusword (0x6041:0, 16 bits) and Position actual value in user units (0x6064:0, 32 bits) 

		self.node.nmt.state = 'OPERATIONAL'
		logging.info("Node nmt state = {}".format(self.node.nmt.state))
		logging.info("Mode of operation = {}".format(self.mode)) # 3

		# ---- SETUP STATE MACHINE --------------------------------------------------------------------------------
		self.node.setup_402_state_machine()
		
		try:
			logging.info("1) Node 402 state = {}".format(self.node.state))
		except:
			logging.error("problem reading node state, please use local version of canopen library (p402.py)")
			raise
			
		self.node.sdo[0x6083].raw = 9800000 # was 8992000, acceleratiom, um/s^2
		self.node.sdo[0x6084].raw = 9800000 # was 8992000, deceleratiom, um/s^2
		self.node.sdo[0x6085].raw =20000000  # quick deceleration
		logging.info("Velocity mode Acceleration    = {:6.3f} m/s^2".format(1e-6*self.node.sdo[0x6083].raw))
		logging.info("Velocity mode Deceleration    = {:6.3f} m/s^2".format(1e-6*self.node.sdo[0x6084].raw))
		logging.info("Velocity mode Quick Stop      = {:6.3f} m/s^2".format(1e-6*self.node.sdo[0x6085].raw))


		self.control = 0x0006  # shutdown
		while self.node.state != "READY TO SWITCH ON":
			# wait for fault clearing
			# pressing green while light is green causes this error after a brief lamps off
			if self.node.state=="FAULT":
				logging.error("fault state: {:x}, statusword = {:x}".
					format(self.node.sdo[0x3518][0].raw, self.status))
				self.control = 0x0080  # fault reset: FAULT -> SWITCH ON DISABLED
			if self.node.state=="SWITCH ON DISABLED": 
				logging.info("switch on disabled: statusword = {:x}".format(self.status))
				self.control = 0x0006  # shutdown:  SWITCH ON DISABLED -> "READY TO SWITCH ON":
			time.sleep(1)
		logging.info("statusword = 0x{:x}".format(self.status))
		logging.info("2) Node 402 state = {}".format(self.node.state))
		self.control = 0x0007  # switch on, does nothing if green button not pressed
		while format(self.node.state) != "SWITCHED ON":
			# wait for green button press
			time.sleep(0.01)
		logging.info("statusword = 0x{:x}".format(self.status))
		logging.info("3) Node 402 state = {}".format(self.node.state))
		self.control = 0x000f  # enable operation
		while format(self.node.state) != "OPERATION ENABLED":
			time.sleep(0.01)
		logging.info("statusword = 0x{:x}".format(self.status))
		logging.info("4) Node 402 state = {0}".format(self.node.state))
		self.control = 0x010F  # Intermediate Stop (bit8=1: halt)
		logging.info("statusword = 0x{:x}".format(self.status))
		logging.info("Node booted up")
		
	def __enter__(self):
		# self.init_velocity() # should not be necessary, don't do it here if necessary in case
		# context manager is not used
		# initialisation should happen before the first motion causing command
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		if self.node.op_mode == 'PROFILED POSITION':
			try:
				self.stop_sinusoid()
			except:
				pass
		elif self.node.op_mode == 'PROFILED VELOCITY':
			self.velocity(0)

		self.network.sync.stop()
		self.network.disconnect()
		
	def init_home(self, force=False):
		"""
		Home if not homed yet. Call this before PP move but not before V move
		"""
		# save old mode
		old_mode = self.node.op_mode

		# check if it is homed already (check only works in homing mode)
		self.node.op_mode="HOMING"

		if self.status & 0x1000:
			logging.info("Already homed: position = {:10.6} m".format(self.x))
			if not force:
				self.node.op_mode = old_mode
				return
		else:
			logging.info("sled not homed: position = {:10.6} m, status: {:04x}".format(self.x, self.status))

		logging.info("Homing method: {:d}".format(self.node.sdo[0x6098].raw))

		self.control = 0x1F | 0x20 # bit 4 starts homing
		t0 = time.time()
		time.sleep(0.1)
		while(time.time()-t0<self.homing_timeout and not self.status & 0x3000):
			time.sleep(1)
		if self.status & 0x1000:
			logging.info("homing success")
		elif self.status & 0x2000:
			logging.error("homing error, status: {:04x}".format(self.status))
		else:
			logging.error("homing timeout? status: {:b} ".format(self.status))
			# 29 s is maximum i have seen and only happens if sled start at x=-1 cm

		self.control = 0x2F # enable operation command + CHANGE_SET_IMMEDIATELY Ivar does this
		self.node.op_mode = old_mode

	def init_velocity(self):
		if self.node.op_mode == 'PROFILED VELOCITY':
			if self.virgin:
				self.control = 0x000F
			return

		logging.info("Switching to velocity mode")
		self.node.op_mode = 'PROFILED VELOCITY'
		logging.info("Mode of operation = {}".format(self.mode))

		# Start motor servo loop
		self.control = 0x000F  # Start motor after intermediate stop (bit8 1->0: start)

		logging.info("controlword     = {:04x}".format(self.control))
		logging.info("statusword      = {:04x}".format(self.status))
		logging.info("Node 402 state  = {}".format(self.node.state))

	def velocity(self, v):
		self.init_velocity()
		if abs(v)<=self.max_velocity:
			self.node.sdo[0x60ff].phys = int(1e6*v)  # um/s

	def init_position(self):
		"""
		Initialize position profile mode

		Set the reference point with a homing move if necessary. Unlike what the documentation says
		the reference must be set for both absolute and relative moves.
		"""

		# check manufacturer status register to see if reference point has been set
		# the status word bit 12 is not useful for this check, it is always zero
		#if not (self.node.sdo[0x1002].raw & 0x02): # bit 1: 1 = reference point set
		self.init_home() # optional homing

		if self.node.op_mode == 'PROFILED POSITION':
			return
	
		logging.info("Switching to profile position mode")
		self.node.op_mode = 'PROFILED POSITION' # mode 1
		logging.info("Mode of operation = {}".format(self.mode))

		# Start motor servo loop
		self.control = 0x000F  # Start motor after intermediate stop (bit8 1->0: start)

		logging.info("controlword     = {:04x}".format(self.control))
		logging.info("statusword      = {:04x}".format(self.status))
		logging.info("Node 402 state  = {}".format(self.node.state))
		
	def set_motion_task(self, task, dt, x=0, table=0, next_task=0, relative=False):
		"""
		set up motion task
		task:      task number (201-300)
		dt:        total move time (s)
		p:         destination position (m)
		table:     table number (0:sinusoid, 1:rsinusoid, 2:minjerk, 3:rev.rsinusoid (ends with minjerk))
		next_task: next task number (0 or 201-300) 
		We use the DS402 versions of these variables if they exist and the ASCII versions (0x3...) otherwise
		"""
		self.node.sdo[0x607A].raw = x * 1e6

		# set control, word 6040
		control = 0
		if relative:
			# This setting overrides control word bit 6 always!
			control |= 0x01 | 0x04 # pos_relative_actual
		if next_task:
			control |= 0x08 # Set bit 3 to indicate next motion task
			control |= 0x10 # velocity blending after
		control |= 0x200 # table (not trajectory generator)
		control |= 0x2000 # si units
		self.node.sdo[0x35B9][1].raw = control #next_task ? 2218 : 2200

		#logging.info("control word O_C : {:x}".format(self.node.sdo[0x35B9][1].raw)) # control variable

		ms = max(2, int(dt*1e3)) # total move time in integer milliseconds
		
		logging.info("motion task {:d}: {} ms acc, {} ms dec, {:d} table, next: {}".format(
			task, ms//2, ms - ms//2 - 1, table, next_task))
		self.node.sdo[0x6083].raw = ms//2 #0x35b7
		self.node.sdo[0x6084].raw = ms - ms//2 - 1 # 1 ms between acc and dec, 0x35ba
		self.node.sdo[0x35B8][1].raw = table # table
		self.node.sdo[0x35BC][1].raw = next_task
		self.node.sdo[0x2082].raw = 0x010000*task # copy from task 0 to this task number
		time.sleep(0.1)

	def move(self, x, dt=None, relative=False, sync=True):
		"""
		Move to position with a minimum jerk profile
		"""

		# initialize position. self.x may be invalid before this line
		self.init_position()

		# calculate move time if none given, init_position needed so self.x is valid
		if dt==None:
			v_ref = 0.10 # m/s, note that peak velocity (force=True)is higher
			if relative:
				dt = abs(x)/v_ref
			else:
				dt = abs(x-self.x)/v_ref
			dt = max(dt, 0.5) # prevent instability in very small moves

		# don't move if sled is at this point already
		if (relative and abs(x) < self.minimum_move) or (not relative and abs(x-self.x) < self.minimum_move):
			logging.info("not moving sled, is already at {:.3f} m.".format(self.x))

		logging.info("moving sled from {:.3f} m {}{:.3f} m in {:.3f} s.".format(self.x, ("to ", "")[relative], x, dt))

		self.set_motion_task(205, dt=dt, x=x, table=2, relative=relative) # minimum jerk
		#self.control = 0x07 # switch on -> SWITCHED ON
		self.control = 0x0F # enable operation p57 -> OPERATION ENABLED
		time.sleep(.1) # necessary, no motion without this one
		self.node.sdo[0x2080].raw = 205 # select motion task
		time.sleep(.1) # necessary, no motion without this one
		self.control = 0x3F # enable operation command + new SETPOINT + CHANGE_SET_IMMEDIATELY
		self.control = 0x2F # enable operation command + CHANGE_SET_IMMEDIATELY Ivar does this
		t0 = time.time()

		# wait for move to end if synchronous operation is desired
		if sync:
			time.sleep(.1) # necessary, otherwise status will still be 'target reached'
			while not self.status&0x400 and time.time()-t0<dt+self.move_timeout:
				time.sleep(0.1)
			else:
				if self.status&0x400:
					logging.info("target reached, x = {:.3f} m".format(self.x))
				else:
					logging.error("move timeout, x = {:.3f} m".format(self.x))
			time.sleep(0.5) # prevent timeouts on next move if it follows immediately

	def start_sinusoid(self, amplitude, period):
		""" 
		Asynchronous function starting a sinusoidal motion in profile position mode
		todo: too fast motion causes n03* - contouring error exceeded preset limit
		"""
		#next four lines are nexcessary for starting this function again, but i dont know why
		self.init_position()
		self.node.sdo[0x2080].raw = 205 # select motion task
		time.sleep(.1) # necessary, no motion without this one
		self.control = 0x3F # start with new SETPOINT and CHANGE_SET_IMMEDIATELY

		self.init_position()

		self.amplitude = amplitude
		self.period = period 
		self.move(-amplitude)
		self.set_motion_task(201, dt=period/2, x=amplitude, table=1, next_task=202) # rsin
		self.set_motion_task(202, dt=period/2, x=-amplitude, table=0, next_task=203) # sin
		self.set_motion_task(203, dt=period/2, x=amplitude, table=0, next_task=202) # sin

		#self.node.sdo[0x6060].raw = 0x01 # mode of operation: position profile = 1, velocity profile = 3, homing = 6
		self.control = 0x0F # enable operation p57 -> OPERATION ENABLED
		time.sleep(.1) # necessary, no motion without this one
		self.node.sdo[0x2080].raw = 201 # select motion task
		time.sleep(.1) # necessary, no motion without this one
		self.control = 0x3F # start with new SETPOINT and CHANGE_SET_IMMEDIATELY

	def stop_sinusoid(self, sync=True):
		"""
		stop sinusoid in position profile mode
		"""
		self.set_motion_task(202, dt=self.period/2, x=-self.amplitude, table=3, next_task=0) #rsin reversed

	# the next 4 getters get the pdo values that are guaranteed to be synced in the 402 profile:
	@property
	def status(self):
		return self.node.tpdo_values[0x6041]
		#return self.node.sdo[0x6041].raw

	@property
	def mode(self):
		try:
			return self.node.tpdo_values[0x6061]
		except KeyError: # not yet synching PDO's 
			return self.node.sdo[0x6061].raw 

	@property
	def x(self):
		""" 
		Get position in m right of calibration point (center).
		"""
		return self.node.tpdo_values[0x6064]*1e-6 # < 1ms
		#return float(self.node.sdo[0x6064].phys) / 1e6 # > 1 ms

	@property
	def v(self):
		""" 
		Get velocity in m/s right.
		"""
		return self.node.tpdo_values[0x606C]*1e-6/self.v_factor # < 1ms>
		#return float(self.node.sdo[0x6064].phys) / 1e6 # > 1 ms

	@mode.setter
	def mode(self, mode):
		self.node.sdo[0x6060].raw = mode

	@property
	def control(self):
		return self.node.sdo[0x6040].raw

	@control.setter
	def control(self, control):
		self.node.sdo[0x6040].raw = control

	# three debug functions
	def print_status(self, all=True):
		states = ("Ready to switch on", "Switched on", "Operation enable",  "Fault", 
			"Voltage enabled", "Quick stop", "Switch on disabled", "Warning",
			"Manufacturer reserved", "Remote", "Target reached", "Internal limit active",
			"Operation mode reserved, homing finished/ position setpoint acknowledge", 
			"Operation mode reserved, homing lag error/ position lag error", 
			"Manufacturer reserved", "Manufacturer reserved")
		print("status: 0x{:04x}, 0b{:08b} {:08b}".format(self.status, self.status>>8, self.status&0xff))
		for i, state in enumerate(states):
			if(all or status & 1<<i):
				print("  {:2d} {:1b} {:s}".format(i, (self.status & 1<<i) != 0, state))

	def print_mstatus(self, all=True):
		"""
		manufacturer status
		"""
		states = ("1 = Movement (positioning, homing) active",
			"1 = reference point set",
			"1 = reference switch high (home-position)",
			"1 = In Position",
			"1 = Position latch at input 2 (positive transition)",
			"reserved",
			"reserved",
			"reserved",
			"reserved",
			"reserved",
			"1 = Initialization phase finished",
			"reserved",
			"1 = Motor stand still message (threshold VEL0)",
			"1 = Safety relay selected (AS-option)",
			"1 = Power stage enabled",
			"1 = Error state",
			"1 = Homing move active",
			"1 = Jog move active",
			"1 = position latch at input 2 (negative transition)",
			"1 = Emergency stop active",
			"1 = Position latch at input 1 (positive transition)",
			"1 = Position latch at input 1 (negative transition)",
			"1 = Feed forward off",
			"1 = Homing move finished",
			"1 = one of the actual errors will lead to a coldstart of the drive, if resetted",
			"1 = digital input 1 set",
			"1 = digital input 2 set",
			"1 = digital input 3 set",
			"1 = digital input 4 set",
			"1 = digital input hardware enable set",
			"reserved",
			"reserved")
		status = self.node.sdo[0x1002].raw
		print("manufacturer status 1002: 0x{:08x}, 0b{:08b} {:08b} {:08b} {:08b}".
			format(status, status>>24&0xff, status>>16&0xff, status>>8&0xff, status&0xff))
		for i, state in enumerate(states):
			if(all or status & 1<<i):
				print("  {:2d} {:1b} {:s}".format(i, (status & 1<<i) != 0, state))

	def print_warning(self, all=True):
		states = ("n01 - I2t - threshold exhausted", 
			"n02 -regen power reached preset regen power limit", 
			"n03* - contouring error exceeded preset limit", 
			"n04* - nodeguarding monitoring has been activated",
			"n05 -Mains supply phase missing", 
			"n06* - position fall below software limit switch 1", 
			"n07* - position exceeded software limit switch 2", 
			"n08 -faulty motion task", 
			"n09 -no reference point at start of motion task", 
			"n10* -PSTOP limit-switch activated", 
			"n11* -NSTOP limit-switch activated", 
			"n12 -discrepancy between motor number saved in the encoder and the amplifier, motor default values loaded",
			"n13* -expansion card not operating correctly", 
			"n14 -SinCos commutation (wake & shake) not completed, will be canceled automatically when amplifier is"
				"enabled and wake & shake carried out",
			"n15 -table error fault according to speed/current table (with INXMODE 35)", 
			"n16 -Summarized warning for n17 to n31",
			"n17 -CAN-Sync is not logged in (with SYNCSRC = 3)", 
			"n18 -Multiturn overflow: Max. number of turns exceeded)",
			"n19 -Motion task ramps are limited Range overflow on motion task data",
			"n20 -Invalid motion task Invalid motion task / GMT data",
			"n21 -PLC error For details see PLC code, customer specific",
			"n22 -Max. motor temperatur reached "
				"The user can shut down the process before the temperature eror will interrupt the process immediately", 
			"n23 -S300/S700: Sin Cos feedback S300/S700: Warning level reached",
			"n24 -S300/S700: Digital I/O S300/S700: Configuration is not logical")
		status = self.node.sdo[0x2000].raw
		print("warning 0x2000: 0x{:08x}, 0b{:08b} {:08b} {:08b} {:08b}".
			format(status, status>>24&0xff, status>>16&0xff, status>>8&0xff, status&0xff))
		for i, state in enumerate(states):
			if(all or status & 1<<i):
				print("  {:2d} {:1b} {:s}".format(i, (status & 1<<i) != 0, state))
