# sled
This document explains how to use the new 'sled' python library. The first section (*cheat sheet*) gives a short introduction.
The rest gives in depth information

## Cheat sheet
### Connecting the sled
![Canable](image/sled_usb.jpg?raw=true "Canable connecting sled to control computer")

The top cable is a USB-A to USB-B-micro cable. The USB-A connector (not shown) goes into the control computer .
The USB-B-micro connector (marked "B") goes into the canable (shown). In the new setup these are permanently 
connected.

The bottom cable is a CAN cable with a 9 pin D-sub female connector (Marked "CAN"). This connector goes into the canable.

### Power on the sled
![Power switch](image/sled_power.jpg?raw=true "Sled power switch")

Turn the power switch to the '1' position as shown

![Green light](image/sled_green.jpg?raw=true "Sled powered")

Wait for the green light. Now press the green button.

![Red light](image/sled_red.jpg?raw=true "Sled enabled")

After pressing the green button, the red light will show and the sled is ready.

### Power off the sled
![Power switch](image/sled_power.jpg?raw=true "Sled power switch")

Turn quarter left to the "0" position.

### Login to the control computer
    ssh control

### Run your script 
    cd ~/Documents/Johnny/MyExperiment
    ./experiment.py

where experiment.py is something like:

    #!/usr/bin/env python3
    from sled import Sled
    with Sled as sled:
        sled.move(0.20)

### Fault recovery
Note that after a fault occured, for instance stepping on the mats around the sled. The red light will go off, but 
the green button does not suffice to turn it on again. Re-run your script and wait for the *READY TO SWITCH ON* text to appear
before pressing the green button. 

## Hardware
- Connect [canable](https://canable.io/). 
- Use candleLight firmware
- Use sl-can firmware
- Peak can interface

## Installation
### Udev setup for candleLight
After inserting the [canable](https://www.socsci.ru.nl/wilberth/computer/canable.html) 
with candleLight firmware it needs to know how to set up a socket. 

/etc/udev/rules.d/10-can.network.rules

    SUBSYSTEM=="net", KERNEL=="can[0-9]", RUN+="/sbin/ip link set can%n type can bitrate 1000000" 
/etc/udev/rules.d/11-can.network.rules

    SUBSYSTEM=="net", KERNEL=="can[0-9]", RUN+="/sbin/ip link set up can%n"
Now there is a can0
### Usb forwarding
IF you need to run your experiment on a computer different from the one that the sled is connected to
you need to forward the usb device over the network. Assuming that the sled is connected to a computer called \
'server' and you want to execute your experiment on a computer called 'control' you need to do the folowing:
on 'sled':

    sudo usbipd&
    usbip list -l # ussuming the USB device is named '3-6.1.1'
    sudo usbip bind -b 3-6.1.1
    
on 'control':
    usbip list -r sled
    usbip attach -r sled -b 3-6.1.1

Now you have a device showing up as '1d50:606f OpenMoko, Inc.' when typing 'lsusb'. This is the sled. You can use it.
### Canopen
Install canopen:

    sudo pip3 install canopen

Replace '/usr/local/lib/python3.6/dist-packages/canopen/profiles/p402.py' with the altered one that does 
not use rpdo's 
### Copy
Copy 'sled.py' to '/usr/local/lib/python3.6/dist-packages' and make sure 's300s700.eds' is reachable.

## Initialisation
- fault recovery

## Demonstration code
- velocity mode
- position mode
- sinoisoid (beyond-402 position mode)
- second client

## legacy
After using the new system, please change things back to the old setup:

- Power off the sled.
- Disconnect the can cable from the canable.
- Connect the can cable to the old sled server (the computer marked 'sled'.
- Restart the sled server:

    ssh sled

    sudo service sled restart
    
- Power on the sled.

## Reasons for this change
There are basically four reasons for this change in software:

- sled-server hardware needs replacement
- client library must be changed to python 3
- wish for replacing peak can with socket can
- wish for adding velocity mode
